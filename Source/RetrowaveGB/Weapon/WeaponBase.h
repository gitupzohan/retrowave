// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "WeaponBase.generated.h"



UCLASS()
class RETROWAVEGB_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float FireDistance = 20000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float BaseDamage = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float RateFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugFire")
		bool ShowDebug = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugFire")
		float SizeVectorToChangeShotDirectionLogic = 100.0f;

	//Dispersion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		bool ShouldReduceDispersion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersionMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersionMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersionRecoil = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float CurrentDispersionReduction = 0.1f;

	// Sounds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		class USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		class USoundBase* SoundReloadWeapon = nullptr;

	//FX
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		class UParticleSystem* EffectFireWeapon = nullptr;

	FVector ShotEndLocation = FVector(0);
	FTimerHandle StateFiring_Timer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void Fire();
	UFUNCTION()
		void FireTimer(bool firing);

	//fire end location
	void ChangeDispersionByShot();
	float GetCurrentDispersion()const;
	FVector ApplyDispersionToShot(FVector DirectionShot)const;
	FVector GetFireEndLocation()const;
};
