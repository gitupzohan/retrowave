// Fill out your copyright notice in the Description page of Project Settings.


#include "RetrowaveGB/Weapon/WeaponBase.h"
#include "RetrowaveGB/Character/RetrowaveGBCharacter.h"
#include "RetrowaveGB/EnemyCharacter/EnemyCharacterBase.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Color.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/DecalComponent.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::Fire()
{
	FVector SpawnLocation = ShootLocation->GetComponentLocation();
	FVector EndLocation = GetFireEndLocation();
	FHitResult Hit;
	TArray<AActor*> Actors;
	UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation,
		ETraceTypeQuery::TraceTypeQuery1, false, Actors, EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
	
	if (ShowDebug)
		DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * FireDistance, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

	// Sound/FX
	if(SoundFireWeapon)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFireWeapon, ShootLocation->GetComponentLocation());
	if(EffectFireWeapon)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectFireWeapon, ShootLocation->GetComponentTransform());

	// damage
	AEnemyCharacterBase* Enemy = Cast<AEnemyCharacterBase>(GetOwner());
	if (Hit.GetActor() && Hit.GetActor() != Enemy)
		UGameplayStatics::ApplyPointDamage(Hit.GetActor(), BaseDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
}

void AWeaponBase::FireTimer(bool firing)
{ 
	if (firing)
		GetWorldTimerManager().SetTimer(StateFiring_Timer,this,&AWeaponBase::Fire, RateFire, true,0.0f);
	else
		GetWorldTimerManager().ClearTimer(StateFiring_Timer);
}

void AWeaponBase::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponBase::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponBase::ApplyDispersionToShot(FVector DirectionShot) const
{
	return FMath::VRandCone(DirectionShot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponBase::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShotEndLocation);

	// if is player
	if (ARetrowaveGBCharacter* Character = Cast<ARetrowaveGBCharacter>(GetOwner()))
	{
		EndLocation = Character->GetCursorToWorld()->K2_GetComponentLocation();

		// set new direction rotate on cursor
		FVector NewDirection = UKismetMathLibrary::GetForwardVector(UKismetMathLibrary::FindLookAtRotation(ShootLocation->GetComponentLocation(), EndLocation));
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShot(NewDirection) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), FireDistance, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	// if is NPC
	if (AEnemyCharacterBase* Character = Cast<AEnemyCharacterBase>(GetOwner()))
	{
		EndLocation = Character->TargetLocation;

		// set new direction rotate on Player
		FVector NewDirection = UKismetMathLibrary::GetForwardVector(UKismetMathLibrary::FindLookAtRotation(ShootLocation->GetComponentLocation(), EndLocation));
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShot(NewDirection) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), FireDistance, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShotEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}
	return EndLocation;
}

