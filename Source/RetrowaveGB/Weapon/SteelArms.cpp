// Fill out your copyright notice in the Description page of Project Settings.


#include "RetrowaveGB/Weapon/SteelArms.h"

// Sets default values
ASteelArms::ASteelArms()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	SphereDamage = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Damage"));
	SphereDamage->SetGenerateOverlapEvents(true);
	SphereDamage->SetCollisionProfileName(TEXT("NoCollision"));
	SphereDamage->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASteelArms::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASteelArms::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

