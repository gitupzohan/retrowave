// Copyright Epic Games, Inc. All Rights Reserved.

#include "RetrowaveGB.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RetrowaveGB, "RetrowaveGB" );

DEFINE_LOG_CATEGORY(LogRetrowaveGB)
 