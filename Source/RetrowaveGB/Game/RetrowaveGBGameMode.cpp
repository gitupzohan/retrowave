// Copyright Epic Games, Inc. All Rights Reserved.

#include "RetrowaveGBGameMode.h"
#include "RetrowaveGBPlayerController.h"
#include "RetrowaveGB/Character/RetrowaveGBCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARetrowaveGBGameMode::ARetrowaveGBGameMode()
{
	// Use only with blueprint classes
	PlayerControllerClass = ARetrowaveGBPlayerController::StaticClass();
	DefaultPawnClass = ARetrowaveGBCharacter::StaticClass();
}