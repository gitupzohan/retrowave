// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "RetrowaveGBGameMode.generated.h"

UCLASS(minimalapi)
class ARetrowaveGBGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ARetrowaveGBGameMode();
};



