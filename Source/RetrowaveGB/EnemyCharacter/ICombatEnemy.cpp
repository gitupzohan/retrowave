// Fill out your copyright notice in the Description page of Project Settings.


#include "RetrowaveGB/EnemyCharacter/ICombatEnemy.h"

// Add default functionality here for any IICombatEnemy functions that are not pure virtual.


bool IICombatEnemy::CanAttack()
{
	return false;
}

bool IICombatEnemy::CanMove()
{
	return false;
}

void IICombatEnemy::MeleeAttack()
{

}

void IICombatEnemy::FarAttack(bool firing)
{

}

