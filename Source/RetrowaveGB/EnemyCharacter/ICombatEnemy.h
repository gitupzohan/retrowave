// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ICombatEnemy.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UICombatEnemy : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class RETROWAVEGB_API IICombatEnemy
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION()
		virtual bool CanAttack();
	UFUNCTION()
		virtual bool CanMove();
	UFUNCTION()
		virtual void MeleeAttack();
	UFUNCTION()
		virtual void FarAttack(bool firing);
	

};
