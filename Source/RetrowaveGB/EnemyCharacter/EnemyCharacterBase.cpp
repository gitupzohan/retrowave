// Fill out your copyright notice in the Description page of Project Settings.


#include "RetrowaveGB/EnemyCharacter/EnemyCharacterBase.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "RetrowaveGB/Character/RetrowaveGBCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemyCharacterBase::AEnemyCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AEnemyCharacterBase::CharDead);
	}
}

// Called when the game starts or when spawned
void AEnemyCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	InitWeapon();

	//Setting speed in game mode
	GetCharacterMovement()->MaxWalkSpeed = CustomSpeed;
	
}

// Called every frame
void AEnemyCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetTargetLocation();
}

// Called to bind functionality to input
void AEnemyCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacterBase::SetTargetLocation()
{
	if (ARetrowaveGBCharacter* Target = Cast<ARetrowaveGBCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(),0)))
	{
		TargetLocation = Target->GetCapsuleComponent()->GetComponentLocation();
	}
}

void AEnemyCharacterBase::AttackCharEvent(bool bIsFiring)
{
	if (bIsFiring)
	{
		CanMovePawn = false;
	}
	else
	{
		CanMovePawn = true;
	}
}

void AEnemyCharacterBase::InitWeapon()
{
	if (WeaponClass)
	{
		FVector SpawnLocation = GetMesh()->GetSocketLocation(FName("WeaponSocketRightHand"));
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		ASteelArms* myWeapon = Cast<ASteelArms>(GetWorld()->SpawnActor(WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (myWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
			MeleeWeapon = myWeapon;
		}
	}
}

void AEnemyCharacterBase::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;
	CanAttackPawn = false;
	CanMovePawn = false;
	AttackCharEvent(false);
}

bool AEnemyCharacterBase::CanAttack()
{
	return CanAttackPawn;
}

bool AEnemyCharacterBase::CanMove()
{
	return CanMovePawn;
}

void AEnemyCharacterBase::SetCanAttack(bool value)
{
	CanAttackPawn = value;
}

void AEnemyCharacterBase::SetCanMove(bool value)
{
	CanMovePawn = value;
}

float AEnemyCharacterBase::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		if (HealthComponent)
			HealthComponent->ChangeHealthValue(-DamageAmount);
	}
	return ActualDamage;
}

void AEnemyCharacterShooter::InitWeapon()
{
	if (FarWeapon)
	{
		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		AWeaponBase* myWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(FarWeapon, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (myWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
			ShooterWeapon = myWeapon;
			ShooterWeapon->BaseDamage = BaseDamage;
			ShooterWeapon->RateFire = AttackSpeed;
		}
	}
}

void AEnemyCharacterShooter::AttackCharEvent(bool bIsFiring)
{
	if (bIsFiring)
	{
		CanMovePawn = false;
	}
	else
	{
		CanMovePawn = true;
	}
	if (ShooterWeapon)
	{
		ShooterWeapon->FireTimer(bIsFiring);
	}

}

void AEnemyCharacterShooter::FarAttack(bool firing)
{
	AttackCharEvent(firing);
}
