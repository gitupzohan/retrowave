// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RetrowaveGB/Weapon/SteelArms.h"
#include "RetrowaveGB/Weapon/WeaponBase.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RetrowaveGB/Character/HealthComponent.h"
#include "EnemyCharacterBase.generated.h"

UCLASS()
class RETROWAVEGB_API AEnemyCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacterBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UHealthComponent* HealthComponent;
	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<ASteelArms> WeaponClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		ASteelArms* MeleeWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings", meta = (ExposeOnSpawn = "true"))
		float BaseDamage = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings", meta = (ExposeOnSpawn = "true"))
		float AttackSpeed = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementSettings", meta = (ExposeOnSpawn = "true"))
		float CustomSpeed = 600.0f;



	FVector TargetLocation = FVector(0);

	//AnimDeads
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> DeadsAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> MeleeAttackAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alive")
		bool bIsAlive = true;

		bool CanMovePawn = true;
		bool CanAttackPawn = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
		
		void SetTargetLocation();
	UFUNCTION(BlueprintCallable)
		virtual	void AttackCharEvent(bool bIsFiring);

		virtual	void InitWeapon();
	/*UFUNCTION(BlueprintCallable)
		AWeaponBase* GetCurrentWeapon();*/
	UFUNCTION()
		virtual	void CharDead();

	UFUNCTION(BlueprintCallable)
		virtual bool CanAttack();
	UFUNCTION(BlueprintCallable)
		virtual bool CanMove();
	UFUNCTION(BlueprintCallable)
		virtual void SetCanAttack(bool value);
	UFUNCTION(BlueprintCallable)
		virtual void SetCanMove(bool value);
	UFUNCTION(BlueprintImplementableEvent)
	 void MeleeAttack();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};

UCLASS()
class RETROWAVEGB_API AEnemyCharacterShooter : public AEnemyCharacterBase
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<AWeaponBase> FarWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		AWeaponBase* ShooterWeapon = nullptr;

/*
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;*/

public:
	void InitWeapon() override;
	void AttackCharEvent(bool bIsFiring) override;

	UFUNCTION(BlueprintCallable)
		virtual void FarAttack(bool firing);
};
