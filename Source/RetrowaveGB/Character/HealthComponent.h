// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Stamina, float, Change);



UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class RETROWAVEGB_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float Health = 100.0f;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefDamage = 1.0f;

	float TakenDamage = 0;
	FTimerHandle HealthTimer;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
		virtual void ChangeHealthValue(float ChangeValue);

};

UCLASS()
class RETROWAVEGB_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;

protected:

	float Shield = 100.0f;
	float Stamina = 100.0f;

public:

	//Health==========================================
	void ChangeHealthValue(float ChangeValue) override;

	//Shield=============================================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		bool HaveShield = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UFUNCTION(BlueprintCallable, Category = "Shield")
		float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();

	//Stamina=============================================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		bool HaveStamina = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float CoolDownStaminaRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverRate = 0.1f;

	FTimerHandle TimerHandle_CoolDownStaminaTimer;
	FTimerHandle TimerHandle_StaminaRecoveryRateTimer;

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		float GetCurrentStamina();
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void ChangeStaminaValue(float ChangeValue);
	void CoolDownStaminaEnd();
	void RecoveryStamina();

};
