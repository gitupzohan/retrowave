// Copyright Epic Games, Inc. All Rights Reserved.

#include "RetrowaveGB/Character/RetrowaveGBCharacter.h"
#include "RetrowaveGB/EnemyCharacter/EnemyCharacterBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ARetrowaveGBCharacter::ARetrowaveGBCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/RetrowaveGB/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Init Health component
	HealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ARetrowaveGBCharacter::CharDead);
	}
}

void ARetrowaveGBCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ARetrowaveGBCharacter::SetupPlayerInputComponent(class UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ARetrowaveGBCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ARetrowaveGBCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ARetrowaveGBCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ARetrowaveGBCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ARetrowaveGBCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Pressed, this, &ARetrowaveGBCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Released, this, &ARetrowaveGBCharacter::InputSprintReleased);

	NewInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ARetrowaveGBCharacter::InputJump);
	
}

void ARetrowaveGBCharacter::BeginPlay()
{
	Super::BeginPlay();
	InitWeapon();
}

void ARetrowaveGBCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ARetrowaveGBCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ARetrowaveGBCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ARetrowaveGBCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ARetrowaveGBCharacter::InputJump()
{
	Jump();
}

void ARetrowaveGBCharacter::InputSprintPressed()
{
	if(moveF)
		GetCharacterMovement()->MaxWalkSpeed = 800.0f;
}

void ARetrowaveGBCharacter::InputSprintReleased()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
}

void ARetrowaveGBCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
		MoveForward();

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursor(ECC_Visibility, true, ResultHit);
			float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
		}
	}
}

void ARetrowaveGBCharacter::AttackCharEvent(bool bIsFiring)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->FireTimer(bIsFiring);
	}
}

AWeaponBase* ARetrowaveGBCharacter::GetCurrentWeapon()
{
	if(CurrentWeapon)
		return CurrentWeapon;
	return nullptr;
}

void ARetrowaveGBCharacter::InitWeapon()
{
	if (Weapon)
	{
		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		AWeaponBase* myWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(Weapon, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (myWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
			CurrentWeapon = myWeapon;
			/*myWeapon->WeaponSetting = myWeaponInfo;
			//Remove!!Debug
			myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
			myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
			myWeapon->UpdateStateWeapon(MovementState);
			myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
			myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStartDelegate);
			myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
			myWeapon->OnWeaponFiring.AddDynamic(this, &ATPSCharacter::WeaponFiring);
			myWeapon->WeaponInfo = WeaponAdditionalInfo;*/
			/*if (InventoryComponent)
				CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

			// after switch try reload weapon if needed
			if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
				CurrentWeapon->InitReload();*/
		}
	}
	
}

void ARetrowaveGBCharacter::MoveForward()
{
	FVector Input = GetCharacterMovement()->GetLastInputVector();
	FVector FwdV = GetActorForwardVector();
	moveF = Input.Equals(FwdV, 0.5f);
	if (!moveF)
		InputSprintReleased();
}

void ARetrowaveGBCharacter::TryReloadWeapon()
{

}

void ARetrowaveGBCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;
	GetCursorToWorld()->SetVisibility(false);
	AttackCharEvent(false);
}

float ARetrowaveGBCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		if (HealthComponent)
			HealthComponent->ChangeHealthValue(-DamageAmount);
	}
	return ActualDamage;
}
