// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RetrowaveGB/Weapon/WeaponBase.h"
#include "RetrowaveGB/Character/HealthComponent.h"
#include "RetrowaveGBCharacter.generated.h"

UCLASS(Blueprintable)
class ARetrowaveGBCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARetrowaveGBCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UCharacterHealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<AWeaponBase> Weapon;

	//AnimDeads
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alive")
		bool bIsAlive = true;

protected:
	virtual void BeginPlay() override;

	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	// flags
	bool moveF = true;

	// Inputs
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION()
		void InputJump();
	UFUNCTION()
		void InputSprintPressed();
	UFUNCTION()
		void InputSprintReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	// Movement
	void MovementTick(float DeltaTime);
	void MoveForward();

 	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		AWeaponBase* CurrentWeapon = nullptr;
	
	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		AWeaponBase* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
// 	UFUNCTION(BlueprintCallable)//VisualOnly
// 		void RemoveCurrentWeapon();


	UFUNCTION()
		void CharDead();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};

