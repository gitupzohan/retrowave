// Fill out your copyright notice in the Description page of Project Settings.


#include "RetrowaveGB/Test/NavLinkProxy_Test.h"
#include "NavLinkCustomComponent.h"
#include "NavAreas/NavArea_Default.h"

void ANavLinkProxy_Test::CopyEndPointsFromSimpleLinkToSmartLinkBP()
{
	
	if (PointLinks.Num() && GetSmartLinkComp())
	{
		GetSmartLinkComp()->SetLinkData(PointLinks[0].Left, PointLinks[0].Right, PointLinks[0].Direction);
	}
}
