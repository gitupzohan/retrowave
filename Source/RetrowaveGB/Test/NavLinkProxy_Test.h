// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Navigation/NavLinkProxy.h"
#include "NavLinkProxy_Test.generated.h"

/**
 * 
 */
UCLASS()
class RETROWAVEGB_API ANavLinkProxy_Test : public ANavLinkProxy
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "AI|Navigation")
		void CopyEndPointsFromSimpleLinkToSmartLinkBP();
};
